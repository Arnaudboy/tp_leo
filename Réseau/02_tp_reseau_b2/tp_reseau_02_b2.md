# TP2 : On va router des trucs
# I. ARP
## 1. Echange ARP
### 🌞 Générer des requêtes ARP
### effectuer un ping d'une machine à l'autre
```
$ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=64 time=0.958 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=64 time=0.902 ms
```
### observer les tables ARP des deux machines
node1.net1.tp2
```
$ arp -a
_gateway (10.0.2.2) at 52:54:00:12:35:02 [ether] on enp0s3
? (10.2.1.12) at 08:00:27:9e:dd:b5 [ether] on enp0s8
? (10.2.1.1) at 0a:00:27:00:00:01 [ether] on enp0s8
```
Adresse MAC de node1 = 08:00:27:9e:dd:b5
node2.net1.tp2
```
$ arp -a
_gateway (10.0.2.2) at 52:54:00:12:35:02 [ether] on enp0s3
? (10.2.1.11) at 08:00:27:7f:fb:cc [ether] on enp0s8
? (10.2.1.1) at 0a:00:27:00:00:01 [ether] on enp0s8
```
Adresse MAC de node1 = 08:00:27:7f:fb:cc

La commande `arp -a` nous montre la mac mais également l'interface, afin de vérifier si l'information est vrai -> ip a
On cherche l'interface correspondante (enp0s8).
node1.net1.tp2
```
$ ip a
...
link/ether 08:00:27:7f:fb:cc ...
...
```
node2.net1.tp2
```
$ ip a
...
link/ether 08:00:27:9e:dd:b5 ...
...
```

## 2. Analyse de trames
### 🌞 Analyse de trames

|ordre | type trame | source                  | destination              |
|------|------------|-------------------------|--------------------------|
|  1   | Requête ARP| node2 08:00:27:9e:dd:b5 | Broadcast FF:FF:FF:FF:FF |
|  2   | Réponse ARP| node1 08:00:27:7f:fb:cc | node2 08:00:27:9e:dd:b5  |

# II. Routage
## 1. Mise en place du routage
### 🌞 Activer le routage sur le noeud router.2.tp2
```
[arnaud@router ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success
```

### 🌞 Ajouter les routes statiques nécessaires pour que node1.net1.tp2 et marcel.net2.tp2 puissent se ping
```
[arnaud@node1 ~]$ ping 10.2.2.12                        | [arnaud@marcel ~]$ ping 10.2.1.11
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.        | PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=1.66 ms | 64 bytes from 10.2.1.11: icmp_seq=1 ttl=63 time=0.912 ms
64 bytes from 10.2.2.12: icmp_seq=2 ttl=63 time=1.40 ms | 64 bytes from 10.2.1.11: icmp_seq=2 ttl=63 time=1.43 ms
```

## 2. Analyse de trames
### 🌞 Analyse des échanges ARP

| ordre | type trame  | IP source | MAC source                 | IP destination | MAC destination            |
|-------|-------------|-----------|----------------------------|----------------|----------------------------|
| 1     | Requête ARP | 10.2.1.11 | `node1` `08:00:27:7f:fb:cc`| 10.2.1.255     |Broadcast`ff:ff:ff:ff:ff:ff`|
| 2     | Réponse ARP | 10.2.1.254| `router``08:00:27:6a:b3:c4`| 10.2.1.11      | `node1``08:00:27:7f:fb:cc` |
| 3     | Ping        | 10.2.1.11 | `node1``08:00:27:7f:fb:cc` | 10.2.1.12      |`router``08:00:27:6a:b3:c4` |
| 4     | Requête ARP | 10.2.2.254| `router``08:00:27:e8:d7:73`| 10.2.2.255     |Broadcast`ff:ff:ff:ff:ff:ff`|
| 5     | Réponse ARP | 10.2.2.12 | `marcel``08:00:27:df:dd:b3`| 10.2.2.254     |`router``08:00:27:e8:d7:73` |
| 6     | Ping        | 10.2.2.254| `router``08:00:27:e8:d7:73`| 10.2.1.12      | `marcel``08:00:27:df:dd:b3`|
| 7     | Pong        | 10.2.1.12 | `marcel``08:00:27:df:dd:b3`| 10.2.2.254     | `router``08:00:27:e8:d7:73`|
| 8     | Pong        | 10.2.1.254| `router``08:00:27:6a:b3:c4`| 10.2.1.11      | `node1` `08:00:27:7f:fb:cc`|

## 3. Accès internet

### 🌞 Donnez un accès internet à vos machines

+ **ajoutez une route par défaut à node1.net1.tp2 et marcel.net2.tp2**
```
[arnaud@node1 ~]$ ping 8.8.8.8                        | [arnaud@marcel ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.          | PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=61 time=16.7 ms | 64 bytes from 8.8.8.8: icmp_seq=1 ttl=61 time=20.9 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=61 time=17.2 ms | 64 bytes from 8.8.8.8: icmp_seq=2 ttl=61 time=17.4 ms
```
+ **donnez leur aussi l'adresse d'un serveur DNS qu'ils peuvent utiliser**
```
[arnaud@node1 ~]$ dig                         | [arnaud@marcel ~]$ dig
;; SERVER: 1.1.1.1#53(1.1.1.1)                | ;; SERVER: 1.1.1.1#53(1.1.1.1)

[arnaud@node1 ~]$ ping google.com                    | [arnaud@marcel ~]$ ping pornhub.com
64 bytes from ... (216.58.198.206): ... time=16.5 ms | 64 bytes from ... (66.254.114.41): ... time=15.9 ms
```
### 🌞 Analyse de trames

| ordre | type trame  | IP source | MAC source                 | IP destination | MAC destination            |
|-------|-------------|-----------|----------------------------|----------------|----------------------------|
| 1     | Ping        | 10.2.1.11 | `node1` `08:00:27:7f:fb:cc`| 8.8.8.8        |`router``08:00:27:6a:b3:c4` |
| 2     | Pong        | 8.8.8.8   | `router``08:00:27:6a:b3:c4`| 10.2.1.11      | `node1``08:00:27:7f:fb:cc` |

Je n'ai pas vidé ma table ARP avant de ping et j'ai filtré toutes les trames SSH, seules les trames ICMP sont visible sur tp2_routage_internet.pcap .

# III. DHCP
## 1. Mise en place du serveur DHCP

### 🌞 Sur la machine node1.net1.tp2, vous installerez et configurerez un serveur DHCP (go Google "rocky linux dhcp server").

+ **faites récupérer une IP en DHCP à l'aide de votre serveur à node2**
```
[arnaud@node2 ~]$ ip a
3: enp0s8:
inet 10.2.1.3/24 brd 10.2.1.255
```

### 🌞 Améliorer la configuration du DHCP
+ **ajoutez de la configuration à votre DHCP pour qu'il donne aux clients, en plus de leur IP :**
```
default-lease-time 900;
max-lease-time 10800;
ddns-update-style none;
authoritative;
subnet 10.2.1.0 netmask 255.255.255.0 {
        range 10.2.1.2 10.2.1.253;
        option subnet-mask 255.255.255.0;
        option domain-name-servers 1.1.1.1;
        option routers 10.2.1.254;
}
```

+ **récupérez de nouveau une IP en DHCP sur node2.net1.tp2 pour tester :**
```
[arnaud@node2 ~]$ ip a
3: enp0s8:
inet 10.2.1.3/24 brd 10.2.1.255

[arnaud@node2 ~]$ ping 10.2.1.254
64 bytes from 10.2.1.254: icmp_seq=1 ttl=64 time=0.927 ms
64 bytes from 10.2.1.254: icmp_seq=2 ttl=64 time=0.840 ms

[arnaud@node2 ~]$ ip r
default via 10.2.1.254 dev enp0s8 proto dhcp metric 100
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.3 metric 100

[arnaud@node2 ~]$ dig
;; SERVER: 1.1.1.1#53(1.1.1.1)

[arnaud@node2 ~]$ ping xhamster.com
PING xhamster.com (104.18.155.3) 56(84) bytes of data.
64 bytes from 104.18.155.3 (104.18.155.3): icmp_seq=1 ttl=61 time=19.6 ms
64 bytes from 104.18.155.3 (104.18.155.3): icmp_seq=2 ttl=61 time=18.2 ms
```
## 2. Analyse de trames
### 🌞 Analyse de trames

+ **videz les tables ARP des machines concernées**
+ **lancer une capture à l'aide de tcpdump afin de capturer un échange DHCP**

| ordre | type trame   | IP source | MAC source                 | IP destination | MAC destination            |
|-------|--------------|-----------|----------------------------|----------------|----------------------------|
| 1     | DHCP Discover| 0.0.0.0   | `node2` `08:00:27:9e:dd:b5`| 255.255.255.255|Broadcast`ff:ff:ff:ff:ff:ff`|
| 2     | ARP Request  | 10.2.1.11 | `node1``08:00:27:7f:fb:cc` | 10.2.1.255     |Broadcast`ff:ff:ff:ff:ff:ff`|
| 3     | ARP Reply    | 0.0.0.0   | `node2` `08:00:27:9e:dd:b5`| 10.2.1.11      |`node1``08:00:27:7f:fb:cc`  |
| 4     | DHCP Offer   | 10.2.1.11 | `node1``08:00:27:7f:fb:cc` | 10.2.1.4       |`node2` `08:00:27:9e:dd:b5` |
| 5     | DHCP Request | 0.0.0.0   | `node1``08:00:27:7f:fb:cc` | 255.255.255.255|Broadcast`ff:ff:ff:ff:ff:ff`|
| 6     | DHCP Ack     | 10.2.1.11 | `node1``08:00:27:7f:fb:cc` | 10.2.1.4       |`node2` `08:00:27:9e:dd:b5` |