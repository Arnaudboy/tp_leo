# TP1 - Mise en jambes
# I. Exploration locale en solo
# 1. Affichage d'informations sur la pile TCP/IP locale

## 🌞 Affichez les infos des cartes réseau de votre PC
```
system_profiler SPNetworkDataType

Type: AirPort
BSD Device Name: en0
IPv4 Addresses: 10.33.3.45
MAC Address: 14:7d:da:1e:77:a4
```
Je ne possède pas de port Ethernet 

## 🌞 Affichez votre gateway
Même commande que précedemment, la passerelle se trouve un un peu plus bas.
```
Router: 10.33.3.253
```
En graphique (GUI : Graphical User Interface)
## 🌞 Trouvez comment afficher les informations sur une carte IP (change selon l'OS)
### Trouvez l'IP, la MAC et la gateway pour l'interface WiFi de votre PC

Dans le menu pomme -> Préférences système -> Réseau
Choisir WIFI dans le menu de gauche. En haut à droite il est écrit : Wi-Fi est connecté à WiFi@YNOV et possède l’adresse IP 10.33.3.45.

![Alt text](./wifi.png?raw=true)

Cliquez sur Avancé en bas à droite, l'adresse MAC s'affiche
![Alt text](./MAC.png?raw=true)
Cliquez sur l'onglet TCP/IP, la passerelle est afichée après "ROUTEUR"
![Alt text](./Routeur.png?raw=true)

## 🌞 à quoi sert la gateway dans le réseau d'YNOV ?

Lorsqu'une requête est adressé sur le réseau, si aucun équipement sur le réseau n'est en mesure de répondre à cette requête alors celle-ci emprunte la passerelle. Les requêtes vers internet passent par la passerelle.

# 2. Modifications des informations

# A. Modification d'adresse IP (part 1)
## 🌞 Utilisez l'interface graphique de votre OS pour changer d'adresse IP :

Idem que précédement:
    Dans le menu pomme -> Préférences système -> Réseau -> Avancé -> Onglet TCP/ IP -> Choisir une configuration d'IP manuelle
![Alt text](./Routeur.png?raw=true)

## 🌞 Il est possible que vous perdiez l'accès internet. Que ce soit le cas ou non, expliquez pourquoi c'est possible de perdre son accès internet en faisant cette opération.
Il est possible de perdre l'accès internet si nous choisissons une adresse IP déjà utilisé. Il est également possible qu'un protocole de sécurité nous empêche de choisir une adresse manuellement.

# B. Table ARP
### Depuis la ligne de commande, afficher la table ARP
```
arp -a
    ? (10.33.3.210) at d8:3b:bf:1a:7a:5c on en0 ifscope [ethernet]
    ? (10.33.3.253) at 0:12:0:40:4c:bf on en0 ifscope [ethernet]
```
### Identifier l'adresse MAC de la passerelle de votre réseau, et expliquer comment vous avez repéré cette adresse MAC spécifiquement

Nous avons déjà trouver l'adresse IP de la passerelle (10.33.3.253), l'adresse MAC coresspondante est 0:12:0:40:4c:bf

### Et si on remplissait un peu la table ?
```
arp -a
    ? (10.33.2.8) at a4:5e:60:ed:b:27 on en0 ifscope [ethernet]
    ? (10.33.1.103) at 18:65:90:ce:f5:63 on en0 ifscope [ethernet]
```
# C. nmap
## 🌞Utilisez nmap pour scanner le réseau de votre carte WiFi et trouver une adresse IP libre
### Lancez un scan de ping sur le réseau YNOV
```
Starting Nmap 7.92 ( https://nmap.org ) at 2021-09-13 12:47 CEST
Nmap scan report for 10.33.0.57
Host is up (0.052s latency).
Nmap scan report for 10.33.0.85
Host is up (0.037s latency).
...
```
l'adresse 10.33.0.58 est libre actuellement.

### Affichez votre table ARP
```
arp -a
? (10.33.0.2) at dc:f5:5:ce:ec:f7 on en0 ifscope [ethernet]
? (10.33.0.13) at d2:50:e:26:94:8b on en0 ifscope [ethernet]
? (10.33.0.14) at (incomplete) on en0 ifscope [ethernet]
? (10.33.0.22) at (incomplete) on en0 ifscope [ethernet]
? (10.33.0.24) at (incomplete) on en0 ifscope [ethernet]
? (10.33.0.27) at a8:64:f1:8b:1d:4d on en0 ifscope [ethernet]
...
```

# D. Modification d'adresse IP (part 2)
## 🌞 Modifiez de nouveau votre adresse IP vers une adresse IP que vous savez libre grâce à nmap
Adresse IP modifiée vers 10.33.0.57

### Utilisez un ping scan sur le réseau YNOV
### montrez moi la commande nmap et son résultat
```
nmap -sP 10.33.0.0/22
Starting Nmap 7.92 ( https://nmap.org ) at 2021-09-13 12:50 CEST
Nmap scan report for 10.33.0.57
Host is up (0.032s latency).
Nmap scan report for 10.33.0.85
Host is up (0.11s latency).
Nmap scan report for 10.33.0.95
Host is up (0.016s latency).
Nmap scan report for 10.33.0.111
Host is up (0.039s latency).
...
```
### prouvez en une suite de commande que vous avez une IP choisi manuellement, que votre passerelle est bien définie, et que vous avez un accès internet

Grâce à la commande system_profiler `SPNetworkDataType` j'obtiens :

    
    Addresses: 10.33.3.45
    Configuration Method: Manual
    Router: 10.33.3.253
    

Je suis capable de ping d'autres adresses IP du réseau mais je n'ai pas accès à internet

# II. Exploration locale en duo
```
ping 172.20.10.3
    PING 172.20.10.3 (172.20.10.3): 56 data bytes
    64 bytes from 172.20.10.3: icmp_seq=0 ttl=64 time=57.647 ms
    64 bytes from 172.20.10.3: icmp_seq=1 ttl=64 time=72.673 ms
```
Nous avons "créer" un réseau, les deux machines se ping.

### affichez et consultez votre table ARP
```
~ arp -a
    ? (172.20.10.1) at ba:5d:a:97:d1:64 on en0 ifscope [ethernet]
    ? (172.20.10.3) at 8c:85:90:d:d:96 on en0 ifscope [ethernet]
    ? (172.20.10.7) at 14:7d:da:1e:77:a4 on en0 ifscope permanent [ethernet]
    ? (224.0.0.251) at 1:0:5e:0:0:fb on en0 ifscope permanent [ethernet]
```
# 5. Petit chat privé

Je suis client. Je vais donc envoyer des paquets sur le port ouvert du server.
```
~ nc 172.20.10.3 8888
 ta mere est tellement grosse que cette blague va être trop longue
 ta mere est tellement grosse que tout le monde la refuse à mcdo
 ta mere est tellement grosse qu'elle a une telecommande pour sa telecommande
 leo le boss (+1 ?)
```

## 🌞 pour aller un peu plus loin

En précisant l'adresse IP (ici la loopback) on obtient le même résultat.
```
~ nc -l 127.0.0.1 8888
    coucou
```
```
~ nc 127.0.0.1 8888
    coucou
```

# 6. Firewall

Après l'activation du Firewall (en GUI), il faut veiller à ce que le mode furtif soit désactiver afin que les ping soient autorisés
![Alt text](./Firewall.png?raw=true)
MacOS n'autorise pas de gestion fine du firewall mais un fichier /etc/pf.conf permet de gérer les protocoles et les ports autorisés en précisant même l'interface. Ainsi en ajoutant au fichier :
```
pass out inet proto icmp 
pass out inet tcp to port 2000
```

Puis en entrant la commande `sudo pfctl -f /etc/pf.conf`, pf n'autorisera les transfert tcp que sur le port 2000 (netcat communique grâce au protocole tcp) et les ping (paquets icmp, le ping devra se faire sur une ipV4 les paquets icmpV6 seront bloqués). 

En toute honnêteté je ne suis pas sûr de comprendre ce que je fais. Packet filter semble différent du firewall et pourtant semble fonctionner de la même manière.

# III. Manipulations d'autres outils/protocoles côté client
# 1. DHCP

## 🌞Exploration du DHCP, depuis votre PC
```
~ ipconfig getpacket en0
    server_identifier (ip): 10.33.3.254
    lease_time (uint32): 0x15180
```
Mon bail expire dans 86400 secondes soit 24 heures

# 2. DNS

## 🌞 trouver l'adresse IP du serveur DNS que connaît votre ordinateur

```
~ scutil --dns
DNS configuration (for scoped queries)

resolver #1
  search domain[0] : auvence.co
  nameserver[0] : 10.33.10.2
  nameserver[1] : 10.33.10.148
  nameserver[2] : 10.33.10.155
```
## 🌞 utiliser, en ligne de commande l'outil nslookup (Windows, MacOS) ou dig (GNU/Linux, MacOS) pour faire des requêtes DNS à la main
```
~ nslookup google.com
Non-authoritative answer:
Name:	google.com
Address: 216.58.201.238
```
```
~ nslookup ynov.com
Non-authoritative answer:
Name:	ynov.com
Address: 92.243.16.143
```

Les adresses IP des servers qui ont effectuer des requêtes sont notés sur les commandes ci-dessus. La commande entière me montre l'adresse de mon DNS, le nom de domaine et l'adresse IP du site que j'ai entré après la commande.
```
~ nslookup 78.74.21.21
Non-authoritative answer:
21.21.74.78.in-addr.arpa	name = host-78-74-21-21.homerun.telia.com.
```
```
~ nslookup 92.146.54.88
88.54.146.92.in-addr.arpa	name = apoitiers-654-1-167-88.w92-146.abo.wanadoo.fr.
```
Ces deux commandes me donnent l'adresse du reverse DNS et le nom de domaine correspondant à l'adresse entrée.

# IV. Wireshark

## 🌞 utilisez le pour observer les trames qui circulent entre vos deux carte Ethernet. Mettez en évidence :

### Un ping entre vous et la passerelle
![Alt text](./Ping.png?raw=true)
### Un netcat entre vous et votre mate, branché en RJ45

J'ai réalisé une commande netcat en écoute sur ma loopback sur le port 8888.
![Alt text](./Netcat.png?raw=true)
### Une requête DNS. Identifiez dans la capture le serveur DNS à qui vous posez la question.
![Alt text](./DNS.png?raw=true)
