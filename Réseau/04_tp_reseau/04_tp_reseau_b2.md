# TP4 : Vers un réseau d'entreprise (https://gitlab.com/it4lik/b2-reseau-2021/-/tree/main/tp/4)

## I. Dumb switch

### 3. Setup topologie 1

```
PC1> ip 10.1.1.1/24                                     | PC2> ip 10.1.1.2/24
PC1> ping 10.1.1.2                                      | PC2> ping 10.1.1.1
                                                        |
84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=18.785 ms | 84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=19.236 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=24.770 ms | 84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=38.031 ms
84 bytes from 10.1.1.2 icmp_seq=3 ttl=64 time=9.189 ms  | 84 bytes from 10.1.1.1 icmp_seq=3 ttl=64 time=12.840 ms
```

## II. VLAN
Configuration du switch
```
Switch>enable
Switch#config t
Switch(config)#interface Gi0/0
(config-if)# switchport mode access
(config-if)# switchport access vlan 10
(config-if)# exit 
Switch(config)#interface Gi0/1
(config-if)# switchport mode access
(config-if)# switchport access vlan 20
(config-if)# exit
Switch(config)#interface Gi0/2
(config-if)# switchport mode access
(config-if)# switchport access vlan 20
(config-if)# exit
```
À mon pc1 (vlan10) je donne l'ip 10.1.1.1
À mes pc2 et pc3 (vlan 20) je donne réspectivement les ip 10.1.1.2 et 10.1.1.3
```
PC1> ping 10.1.1.2           | PC2> ping 10.1.1.3                                     | PC3> ping 10.1.1.2

host (10.1.1.2) not reachable| 84 bytes from 10.1.1.3 icmp_seq=1 ttl=64 time=13.096 ms| 84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=20.502ms
                             | 84 bytes from 10.1.1.3 icmp_seq=2 ttl=64 time=5.770 ms | 84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=7.938 ms
```
PC1 ne peut pas ping les autres pc du réseau ; à l'intérieur de la VLAN 20 les pc peuvent se ping.
DISCLAIMER : La consigne demande que pc3 soit isolé, j'ai isolé PC1... déso :'(.

## III. Routing

À l'ajout des machines j'ai créé leurs adresses IP avec ip (ip machine) (netmask) (gateway) pour web1 j'ai ajouté une route par défaut avec `sudo ip route add default via 10.3.3.254`
J'ai configuré le switch comme précédemment pour les ports de PC1, PC2, adm1 et web1. La manip est la même seul les noms et numéros des vlan changent.
Pour résumer :
    - on entre en mode config avec `enable` puis `conf t`
    - on entre en monde conf-vlan avec `vlan (numéro de la vlan>`
    - on nomme la vlan
    - `exit`
On configure les ports du switch sur lesquels sont branchés les clients comme présenté dans le II. C'est le mode access qu'on vient de configurer, il faut maintenant configurer le mode trunk.
```
# conf t
(config)# interface fastEthernet0/0
(config-if)# switchport trunk encapsulation dot1q
(config-if)# switchport mode trunk
(config-if)# switchport trunk allowed vlan add 11,13
(config-if)# switchport trunk allowed vlan add 12
(config-if)# exit
(config)# exit
```
Configuration du routeur : Il faut attribuer plusieurs adresses sur le port utilisé pour communiquer avec sw1, chez moi f0/0
```
# conf t

(config)# interface f0/0.11
R1(config-subif)# encapsulation dot1Q 11
R1(config-subif)# ip addr 10.1.1.254 255.255.255.0
R1(config-subif)# exit

(config)# interface f0/0.12
R1(config-subif)# encapsulation dot1Q 12
R1(config-subif)# ip addr 10.2.2.254 255.255.255.0
R1(config-subif)# exit


(config)# interface f0/0.13
R1(config-subif)# encapsulation dot1Q 13
R1(config-subif)# ip addr 10.3.3.254 255.255.255.0
R1(config-subif)# exit
```
J'ai créé 3 sous-interfaces de f0/0 et j'y ai encapsulé le protocole dot1Q (ou IEEE 802.1Q) le standard supportant les vlans. Les adresses IP sont les adresses de gateway de leur réseau réspectif.
Toutes les machines peuvent maintenant se ping entre elles.

## IV. NAT

Le câble est branché sur eth1 côté cloud.
Côté routeur, configuration du port sur lequel est branché le cloud -> on lui donne une adresse en dhcp.
```
#conf t
(config)#interface f1/0
(config-if)#ip address dhcp
(config-if)#no shutdown
(config-if)#exit
```
Maintenant on configure le NAT
interface relié au cloud :
```
(config)# interface f1/0
(config-if)# ip nat outside
(config-if)# exit
```
interface relié au switch
```
(config)# interface f0/0
(config-if)# ip nat inside
(config-if)# exit
```
Définition d'une liste où le trafic est autorisé (toujours sur le router)
`(config)# access-list 1 permit any`

Configuration du NAT :

`(config)# ip nat inside source list 1 interface f1/0 overload`

On ajoute une route par défaut sur les VPCs et la VM.
VPCs : 
    La commande est : `ip <ip addr> <netmask> <gateway/route par défaut>`

VM :
    On ajoute une route par défaut soit avec la commande `ip route add` soit en créant un fichier route-(interface) dans `/etc/sysconfig/network-scripts/`
On ajoute la possibilité pour les VM d'utiliser un service DNS (je choisi le DNS se trouvant à l'adresse 1.1.1.1).

VPCs :
    La commande `ip dns 1.1.1.1` suffit

VM :
    J'ai édité le fichier de configuration de mon interface.
On ajoute la possibilité pour les VM d'utiliser un service DNS (je choisi le DNS se trouvant à l'adresse 1.1.1.1).

VPCs :
    La commande `ip dns 1.1.1.1` suffit

VM :
    J'ai édité le fichier de configuration de mon interface.
