# I. Prise en main
# 1. Lancer des conteneurs

### Conteneur simple
```
[arnaud@localhost ~]$ docker pull alpine
...
[arnaud@localhost ~]$ docker run alpine
[arnaud@localhost ~]$ docker container ls
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
[arnaud@localhost ~]$ docker container ls -a
CONTAINER ID   IMAGE         COMMAND     CREATED          STATUS                      PORTS     NAMES
6931f0952e74   alpine        "/bin/sh"   42 seconds ago   Exited (0) 42 seconds ago             boring_haslett
795870ff82a3   hello-world   "/hello"    4 minutes ago    Exited (0) 4 minutes ago              hopeful_kepler
```
### Conteneur simple, mais vivant
### Manipulation du conteneur

### 🌞 lister les conteneurs actifs, et mettre en évidence le conteneur lancé sur le sleep
[arnaud@localhost ~]$ docker container ls
CONTAINER ID   IMAGE     COMMAND        CREATED          STATUS          PORTS     NAMES
4dd78bb2eb53   alpine    "sleep 9999"   12 seconds ago   Up 11 seconds             sweet_beaver

ID=4dd78bb2eb53 Nom=sweet_beaver

### 🌞 Mise en évidence d'une partie de l'isolation mise en place par le conteneur. Montrer que le conteneur utilise :

une arborescence de processus différente
```
/ # ps
PID   USER     TIME  COMMAND
    1 root      0:00 sleep 9999
    7 root      0:00 sh
   13 root      0:00 ps
```
des cartes réseau différentes
```
/ # ifconfig
eth0      Link encap:Ethernet  HWaddr 02:42:AC:11:00:02
          inet addr:172.17.0.2  Bcast:172.17.255.255  Mask:255.255.0.0
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
[...]

lo        Link encap:Local Loopback
          inet addr:127.0.0.1  Mask:255.0.0.0
          UP LOOPBACK RUNNING  MTU:65536  Metric:1
...
```
des utilisateurs système différents
```
/ # cat /etc/passwd
root:x:0:0:root:/root:/bin/ash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
...
```
des points de montage (les partitions montées) différents
```
/ # df -h
Filesystem                Size      Used Available Use% Mounted on
overlay                   6.2G      2.4G      3.8G  39% /
tmpfs                    64.0M         0     64.0M   0% /dev
tmpfs                   404.7M         0    404.7M   0% /sys/fs/cgroup
shm                      64.0M         0     64.0M   0% /dev/shm
...
```
### 🌞 détruire le conteneur avec docker rm

```
$ docker rm -f 4dd78bb2eb53
4dd78bb2eb53
```
La commande `docker container ls` n'affiche plus de container.

### 🌞 Lancer un conteneur NGINX
### utiliser l'image nginx

![Alt text](./docker.png?raw=true)

# 2. Gestion d'images

### Gestion d'images
### 🌞 récupérer une image de Apache en version 2.2
```
$ docker run -d -p 80:80 httpd:2.2.29
$ docker container ls
CONTAINER ID   IMAGE          COMMAND              CREATED          STATUS          PORTS                               NAMES
a0eb07d6a3bf   httpd:2.2.29   "httpd-foreground"   21 seconds ago   Up 20 seconds   0.0.0.0:80->80/tcp, :::80->80/tcp   lucid_dubinsky
```
Si je lance un curl depuis mon host
```
~ curl 10.10.1.20:80
<html><body><h1>It works!</h1></body></html>%
```

### Création d'image
### 🌞 créer une image qui lance un serveur web python. L'image doit :

```# Image de base
FROM alpine

# Ajout d'une application dans le conteneur
RUN apk update && apk add python3

# expose
EXPOSE 8888/tcp

WORKDIR /home/arnaud

COPY partage .

CMD python3 -m http.server 8888
```
### 🌞 lancer le conteneur et accéder au serveur web du conteneur depuis votre PC
![Alt text](./python_server.png?raw=true)

### 🌞 utiliser l'option -v de docker run

```docker run -d -v /home/arnaud/test/:/home/arnaud/dossier -p 80:8888 image1```
![Alt text](./run-v.png?raw=true)

# 3. Manipulation du démon docker
### 🌞 Modifier la configuration du démon Docker :

### prouver que ça fonctionne en manipulant le démon Docker à travers le réseau (depuis une autre machine)
```
docker -H 10.10.1.20:2375 ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
```
Aucun processus n'est lancé pour le moment. Il m'a fallu coupé mon firewall pour accéder au démon de Docker depuis une autre machine.

### modifier l'emplacement des données Docker
J'ai créé un fichier daemon.json dans le root dir par défaut (/var/lib/docker) dans lequel j'ai précisé le path vers le nouveau data-root
```
{
"data-root": "/home/arnaud/docker"
}
```
Puis j'ai redémarrer le service et vérifier que le nouveau data root était bien pris en compte.
```
$ docker info | grep "Docker Root Dir"
 Docker Root Dir: /home/arnaud/docker
```
