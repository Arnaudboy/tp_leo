# TP1 : (re)Familiaration avec un système GNU/Linux
# 0. Préparation de la machine


### 🌞 Setup de deux machines Rocky Linux configurées de façon basique.

+ **un accès internet (via la carte NAT):**
```
[arnaud@node1 ~]$ ping 8.8.8.8                          | [arnaud@node2 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.            | PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=63 time=27.3 ms   | 64 bytes from 8.8.8.8: icmp_seq=1 ttl=63 time=26.9 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=63 time=24.8 ms   | 64 bytes from 8.8.8.8: icmp_seq=2 ttl=63 time=24.7 ms
```

+ **un accès à un réseau local** & **les machines doivent pouvoir se joindre par leurs noms respectifs**
```
[arnaud@node1 ~]$ ping node2.tp1.b2                    | [arnaud@node2 ~]$ ping node1.tp1.b2
PING node2.tp1.b2 (10.101.1.12) 56(84) bytes of data.  | PING node1.tp1.b2 (10.101.1.11) 56(84) bytes of data.
... from node2.tp1.b2 (10.101.1.12): ... time=0.390 ms | ...from node1.tp1.b2 (10.101.1.11): ... time=1.06 ms
                                                       | ... from node1.tp1.b2 (10.101.1.11): icmp_seq=2 ttl=64 time=0.744 ms
```
+ **les machines doivent avoir un nom**
```
[arnaud@node1 ~]$ hostname | [arnaud@node2 ~]$ hostname
node1.tp1.b2               | node2.tp1.b2
```
+ **utiliser 1.1.1.1 comme serveur DNS**
```
[arnaud@node1 ~]$ dig ynov.com                | [arnaud@node2 ~]$ dig ynov.com
;; ANSWER SECTION:                            | ;; ANSWER SECTION:
ynov.com.		8221	IN	A	92.243.16.143 | ynov.com.		4474	IN	A	92.243.16.143
;; SERVER: 1.1.1.1#53(1.1.1.1)                | ;; SERVER: 1.1.1.1#53(1.1.1.1)
```
+ **les machines doivent pouvoir se joindre par leurs noms respectifs**
```
[arnaud@node1 ~]$ ping node2.tp1.b2                                       | [arnaud@node2 ~]$ ping node1.tp1.b2
PING node2.tp1.b2 (10.101.1.12) 56(84) bytes of data.                     | PING node1.tp1.b2 (10.101.1.11) 56(84) bytes of data.
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=1 ttl=64 time=0.986 ms | 64 bytes ...(10.101.1.11): icmp_seq=1 ttl=64 time=0.543 ms
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=2 ttl=64 time=1.13 ms  | 64 bytes ...(10.101.1.11): icmp_seq=2 ttl=64 time=0.692 ms
```
+ **le pare-feu est configuré pour bloquer toutes les connexions exceptées celles qui sont nécessaires**
```
[arnaud@node1 ~]$ sudo firewall-cmd --list-all | [arnaud@node2 ~]$ sudo firewall-cmd --list-all
public (active)                                | public (active)
  target: default                              |   target: default
  icmp-block-inversion: no                     |   icmp-block-inversion: no
  interfaces: enp0s3 enp0s8                    |   interfaces: enp0s3 enp0s8
  sources:                                     |   sources:
  services: cockpit dhcpv6-client ssh          |   services: cockpit dhcpv6-client ssh
  ports:                                       |   ports:
  protocols:                                   |   protocols:
  masquerade: no                               |   masquerade: no
  forward-ports:                               |   forward-ports:
  source-ports:                                |   source-ports:
  icmp-blocks:                                 |   icmp-blocks:
  rich rules:                                  |   rich rules:
```
# I. Utilisateurs
## 1. Création et configuration
### 🌞 Ajouter un utilisateur à la machine, qui sera dédié à son administration. Précisez des options sur la commande d'ajout pour que :
+ **le répertoire home de l'utilisateur soit précisé explicitement, et se trouve dans /home**
```
[arnaud@node1 ~]$ sudo useradd toto -d /home/toto -s /bin/bash | [arnaud@node2 ~]$ sudo useradd toto -d /home/toto -s /bin/bash
[[arnaud@node1 ~]$ cat /etc/passwd | grep toto                 | toto@node2 ~]$ cat /etc/passwd | grep toto
toto:x:1001:1001::/home/toto:/bin/bash                         | toto:x:1001:1001::/home/toto:/bin/bash
```
### 🌞 Créer un nouveau groupe admins qui contiendra les utilisateurs de la machine ayant accès aux droits de root via la commande sudo.
Sur node1 et node2 j'ai ajouté les lignes suivantes au fichier sudoers :
```
## Allows people in group admin to run all commands
%admin  ALL=(ALL)       ALL
```
### 🌞 Ajouter votre utilisateur à ce groupe admins.
```
[arnaud@node1 ~]$ sudo usermod -aG admin toto | [arnaud@node2 ~]$ sudo usermod -aG admin toto
[arnaud@node1 ~]$ groups toto                 | [arnaud@node2 ~]$ groups toto
toto : toto admin                             | toto : toto admin
```
## 2. SSH
### 🌞 Pour cela :
+ **il faut générer une clé sur le poste client de l'administrateur qui se connectera à distance (vous :) )**
Pour générer une clé on entre la commande suivante sur la machine hôte.
```
(base) ➜  ~ ssh-keygen -t rsa
```
+ + **déposer la clé dans le fichier /home/<USER>/.ssh/authorized_keys de la machine que l'on souhaite administrer**
Pour copier les clés ssh j'ai entré les commandes suivantes.
```
(base) ➜  ~ ssh-copy-id arnaud@10.101.1.11
(base) ➜  ~ ssh-copy-id arnaud@10.101.1.12
```
### 🌞 Assurez vous que la connexion SSH est fonctionnelle, sans avoir besoin de mot de passe.
```
(base) ➜  ~ ssh arnaud@10.101.1.11                   | (base) ➜  ~ ssh arnaud@10.101.1.12
Activate the web console with: ...                   | Activate the web console with: systemctl enable --now cockpit.socket
Last login: Sun Sep 26 12:53:33 2021 from 10.101.1.1 | Last login: Sun Sep 26 14:21:15 2021 from 10.101.1.1
[arnaud@node1 ~]$                                    | [arnaud@node2 ~]$
```
# II. Partitionnement
## 2. Partitionnement
### 🌞 Utilisez LVM :
```
[arnaud@node2 ~]$ lsblk
NAME                        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda                           8:0    0    8G  0 disk
├─sda1                        8:1    0    1G  0 part /boot
└─sda2                        8:2    0    7G  0 part
  ├─rl_bastion--ovh1fr-root 253:0    0  6,2G  0 lvm  /
  └─rl_bastion--ovh1fr-swap 253:1    0  820M  0 lvm  [SWAP]
sdb                           8:16   0    3G  0 disk
sdc                           8:32   0    3G  0 disk
sr0                          11:0    1 1024M  0 rom
[arnaud@node2 ~]$ sudo pvcreate /dev/sdb
  Physical volume "/dev/sdb" successfully created.
[arnaud@node2 ~]$ sudo pvcreate /dev/sdc
  Physical volume "/dev/sdc" successfully created.

[arnaud@node2 ~]$ sudo vgcreate data /dev/sdb /dev/sdc
  Volume group "data" successfully created
[arnaud@node2 ~]$ sudo vgs
  VG                #PV #LV #SN Attr   VSize  VFree
  data                2   0   0 wz--n-  5,99g 5,99g
  rl_bastion-ovh1fr   1   2   0 wz--n- <7,00g    0

[arnaud@node2 ~]$ sudo lvcreate -L 1024M data -n sdb
[arnaud@node2 ~]$ sudo lvcreate -L 1024M data -n part2
[arnaud@node2 ~]$ sudo lvcreate -L 1024M data -n part3

[arnaud@node2 ~]$ sudo mkfs -t ext4 /dev/data/sdb
[arnaud@node2 ~]$ sudo mkfs -t ext4 /dev/data/part2
[arnaud@node2 ~]$ sudo mkfs -t ext4 /dev/data/part3

[arnaud@node2 ~]$ mkdir /mnt/data-sdb && sudo mount /dev/data/sdb /mnt/data-sdb/
[arnaud@node2 ~]$ mkdir /mnt/data-part2 && sudo mount /dev/data/part2 /mnt/data-part2/
[arnaud@node2 ~]$ mkdir /mnt/data-part3 && sudo mount /dev/data/part3 /mnt/data-part3/
```
### 🌞 Grâce au fichier /etc/fstab, faites en sorte que cette partition soit montée automatiquement au démarrage du système.
```
sudo vi /etc/fstab
[...]
/dev/data/sdb /mnt/data-sdb ext4 defaults 0 0
/dev/data/part2 /mnt/data-part2 ext4 defaults 0 0
/dev/data/part3 /mnt/data-part3 ext4 defaults 0 0
```
L'ajout de ces trois lignes à /etc/fstab suffit à indiquer le montage automatique de ces 3 volumes logiques au démarrage.
# III. Gestion de services
## 1. Interaction avec un service existant
### 🌞 Assurez-vous que :
```
[arnaud@node1 ~]$ systemctl status firewalld
● firewalld.service - firewalld - dynamic firewall daemon
   Loaded: loaded (/usr/lib/systemd/system/firewalld.service; enabled; vendor p>
   Active: active (running) since Sun 2021-09-26 16:33:08 CEST; 30min ago
```
## 2. Création de service
## A. Unité simpliste
### 🌞 Créer un fichier qui définit une unité de service web.service dans le répertoire /etc/systemd/system.
```
[arnaud@node1 ~]$ sudo firewall-cmd --add-port=8888/tcp --permanent
success
[arnaud@node1 ~]$ sudo systemctl status web
● web.service - Very simple web service
   Loaded: loaded (/etc/systemd/system/web.service; enabled; vendor preset: dis>
   Active: active (running) since Sun 2021-09-26 17:17:52 CEST; 1s ago
```
### 🌞Une fois le service démarré, assurez-vous que pouvez accéder au serveur web : avec un navigateur ou la commande curl sur l'IP de la VM, port 8888.
```
(base) ➜  ~ curl 10.101.1.11:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
...
```
## B. Modification de l'unité
### 🌞 Créer un utilisateur web
```
[arnaud@node1 ~]$ sudo useradd web
```
### 🌞 Modifiez l'unité de service web.service créée précédemment en ajoutant les clauses  User= & WorkingDirectory= :
```
[arnaud@node1 ~]$ cat /etc/systemd/system/web.service
[Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python3 -m http.server 8888
User=web
WorkingDirectory=/srv/srvweb
...
```
### 🌞 Placer un fichier de votre choix dans le dossier créé dans /srv et tester que vous pouvez y accéder une fois le service actif. Il faudra que le dossier et le fichier qu'il contient appartiennent à l'utilisateur web.
```
[arnaud@node1 ~]$ touch /srv/test
```
### 🌞 Vérifier le bon fonctionnement avec une commande curl
```
(base) ➜  ~ curl 10.101.1.11:8888/srv/
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
...
<title>Directory listing for /srv/</title>
</head>
<body>
<h1>Directory listing for /srv/</h1>
...
<li><a href="test">test</a></li>
...
```