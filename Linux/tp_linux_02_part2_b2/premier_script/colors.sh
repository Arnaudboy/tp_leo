#!/bin/bash
# Stock color variables
# Arnaud 18/10/21

# Colors
reset="\e[0m"
bold="\e[1m"
italic="\e[3m"
uline="\e[4m"
blink="\e[5m"
crossed="\e[9m"
yellow="\e[33m"
red="\e[91m"
green="\e[92m"
blue="\e[94m"
white="\e[97m"
red_bg="\e[101m"
green_bg="\e[102m"
blue_bg="\e[104m"
expand_bg="\e[K"
