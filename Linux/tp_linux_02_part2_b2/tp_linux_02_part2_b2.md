# TP2 pt. 2 : Maintien en condition opérationnelle

#I. Monitoring

## 2.Setup

🌞 **Setup Netdata**

```
[arnaud@web ~]$ su -
[root@web ~]# dnf install libuuid-devel
[root@web ~]# dnf install zlib-devel
[root@web ~]# dnf install json-c-devel
[root@web ~]# dnf install gcc
[root@web ~]# dnf install make autoconf automake
[root@web ~]# dnf install pkg-config
[root@web ~]# bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
[root@web ~]# exit
```

🌞 **Manipulation du service Netdata**

```
[arnaud@web ~]$ systemctl status netdata
● netdata.service - Real time performance monitoring
   Loaded: loaded (/usr/lib/systemd/system/netdata.service; ena>
   Active: active (running) since Mon 2021-10-11 21:42:27 CEST;>
[arnaud@web ~]$ systemctl is-enabled netdata
enabled
[arnaud@web ~]$ ss -alnpt
State  Recv-Q  Send-Q   Local Address:Port    Peer Address:Port Process
LISTEN 0       128            0.0.0.0:22           0.0.0.0:*
LISTEN 0       128          127.0.0.1:8125         0.0.0.0:*
LISTEN 0       128            0.0.0.0:19999        0.0.0.0:*
LISTEN 0       128                  *:80                 *:*
LISTEN 0       128               [::]:22              [::]:*
LISTEN 0       128              [::1]:8125            [::]:*
LISTEN 0       128               [::]:19999           [::]:*
[arnaud@web ~]$ sudo firewall-cmd --add-port=19999/tcp
success
[arnaud@web ~]$ sudo firewall-cmd --add-port=19999/tcp --permanent
success
```
Le service netdata est en écoute sur le port 19999 en tcp. Un autre processus est en écoute sur ma loopback sur le port 8125 il s'agit de StatsD, le server StatsD est utilisé par netdata, c'est lui qui collecte les metrics.

🌞 **Setup Alerting**

Après avoir créé un "webhook" sur un server discord et un rôle "sysadmin", j'intègre le lien dans le fichier health_alarm_notify.conf -toujours en utilisant vim parce que je suis nul et que je sais toujours pas me servir de sed.

🌞 **Config alerting**

```
[arnaud@web ~]$ cd /opt/netdata/etc/netdata
[arnaud@web ~]$ sudo ./edit-config health.d/ram-usage.conf
```
Epilogue : j'ai copié/collé le fichier de conf de la doc (https://learn.netdata.cloud/guides/step-by-step/step-05) et j'ai modifié les valeurs qui trigger l'alarme pour la placer à 50. J'ai redémarré le service netdata.
```
[arnaud@web ~]$ sudo stress --vm 2 --vm-bytes 700M --timeout 70s
```
ET BIM une notif sur mon server discord. Trop stylé !

# II. Backup

## 2. Partage NFS

La maipulation de création d'un dossier de partage avec le protocole NFS est exactement la même que celle du tp précédent.
```
[arnaud@backup ~]$ sudo vim /etc/idmapd.conf <- on ajoute un nom de domaine
[arnaud@backup ~]$ sudo vim /etc/exports <- on ajoute une adresse ip autorisée à ce connecté en rw
    /srv/backups/web.tp2.linux 10.102.1.11/24(rw,no_root_squash)
[arnaud@backup ~]$ sudo firewall-cmd --add-service=nfs --permanent
```
Ces manipulations sont à effectuées sur le server, ne pas oublier de restart le firewall si utilisation de --permanent.
Go set le client maintenant
**Après installation de nfs-utils**
```
[arnaud@web ~]$ sudo vim /etc/idmapd.conf <- même manip que sur le server (je ne comprends pas l'utilité de cette commande)
[arnaud@web ~]$ sudo mount -t nfs 10.102.1.13:/srv/backups/web.tp2.linux /srv/backup/ <- après création de backup dans srv
```

** Point Bonus : **

Point de montage automatique :
Ajout de la ligne `10.102.1.13:/srv/backups/web.tp2.linux /srv/backup nfs defaults 0 0` à /etc/fstab.

## 3. Backup de fichiers

Le script utilise borg.Il se trouve dans le dossier `premier_script` il comporte 3 fichiers :
    - un fichier colors.sh contenant des variables pour coloriser le script.
    - un fichier contenant une variable `quantity`. Cette variable permet de sauvegarder le nombre d'archives conservées
    - le fichier de backup `tp2_backup.sh`

Trois options sont disponibles :
    - `-h` utilisée seule permet de faire fonctionner la fonction usage
    - `-q`permet de modifier la quantité
    - `-d` obligatoire. Précise le path vers le repository borg

Si le directory précisé n'est pas un repository borg, il sera créé en nommé newRepository, borg se charge de demander d'entrée une passphrase.
Le script vérifie ensuite si tous les arguments de la fonctions sont bien des dossiers et/ou fichiers existant puis crée l'archive en utilisant la date et l'heure de la création comme nom.
La dernière partie calcul la différence entre le nombre d'archives dans le repository et la quantité demandée par l'utilisateur et efface les plus anciennes.

## 4. Unité de service

### A. Unité de service

J'ai dû définir une variable d'environnement `BORG_PASSPHRASE` pour faire fonctionner le service avec Borg. 
Pour des raison de simplicité et pour avancer plus rapidement je l'ai simplement ajouté au script en clair et j'ai modifié les droits en 700 (ainsi seul root peut lire le fichier).
Il est possible avec borg de stocker cette variable de manière chiffré dans un fichier et d'y faire appel dans un script.
Dans `/etc/systemd/system/tp2_backup.service` j'ai inscrit :
```
[Unit]
Description=Our own lil backup service (TP2)

[Service]
ExecStart=/srv/tp2_backup.sh -d /srv/backup/repo /home/arnaud/test
Type=oneshot
RemainAfterExit=no

[Install]
WantedBy=multi-user.target
```
** 🌞 Tester le bon fonctionnement **
```
[arnaud@web ~]$ sudo systemctl daemon-reload
[arnaud@web ~]$ sudo systemctl start tp2_backup.service
[arnaud@web ~]$ sudo systemctl status  tp2_backup.service
● tp2_backup.service - Our own lil backup service (TP2)
   Loaded: loaded (/etc/systemd/system/tp2_backup.service; enabled; vendor preset: disabled)
   Active: inactive (dead) since Mon 2021-11-01 20:16:37 CET; 4s ago
  Process: 2490 ExecStart=/usr/bin/sh /srv/tp2_backup.sh -d /srv/backup/repo/ /home/arnaud/test (code=exited, status=0/SUCCESS)
 Main PID: 2490 (code=exited, status=0/SUCCESS)

nov. 01 20:16:33 web.tp2.linux systemd[1]: Starting Our own lil backup service (TP2)...
nov. 01 20:16:33 web.tp2.linux sh[2490]: A repository already exists at /srv/backup/repo.
nov. 01 20:16:34 web.tp2.linux sh[2490]: A repository already exists at /srv/backup/repo.
nov. 01 20:16:37 web.tp2.linux systemd[1]: tp2_backup.service: Succeeded.
nov. 01 20:16:37 web.tp2.linux systemd[1]: Started Our own lil backup service (TP2).

[arnaud@web ~]$ sudo borg list /srv/backup/repo/
Enter passphrase for key /srv/backup/repo:
tp2_backup_2021-11-01T19:42:25       Mon, 2021-11-01 19:42:41 [9294d86d4173fc96f155831f16a1d3c610c09a8a110b892b7252c4f2e9a828a2]
tp2_backup_2021-11-01T19:43:15       Mon, 2021-11-01 19:43:16 [1fc5576a88db05d029e5c2424e8ca343efc3c6ec3055c4033e67a553d47e3ef6]
tp2_backup_2021-11-01T19:56:54       Mon, 2021-11-01 19:56:56 [75829512fa4cd8c5fa5606eaba873fa5f81f34cb0e6c7c1a1ae143720494af46]
tp2_backup_2021-11-01T20:16:26       Mon, 2021-11-01 20:16:26 [f0b16444cec172f3b9e788e9e2f8bb21e6252f58a22bda03ad2ecdc43c31f6e8]
tp2_backup_2021-11-01T20:16:35       Mon, 2021-11-01 20:16:35 [4ec713e3feb5d2e1cfb528ab4a8de20c4ab4e446c07608b54a913e6cda29076b]
```

### B. Timer

Afin de pouvoir tester le bon fonctionnement de mon timer je choisis un intervalle court (ici 1min).
```
[Unit]
Description=Periodically run our TP2 backup script
Requires=tp2_backup.service

[Timer]
Unit=tp2_backup.service
OnCalendar=*-*-* *:*:00

[Install]
WantedBy=timers.target
```
** 🌞 Activez le timer **
Avec preuve de bon fonctionnement #Fierté
```
[arnaud@web ~]$ sudo systemctl start tp2_backup.timer
[sudo] Mot de passe de arnaud :
[arnaud@web ~]$ sudo systemctl enable tp2_backup.timer
Created symlink /etc/systemd/system/timers.target.wants/tp2_backup.timer → /etc/systemd/system/tp2_backup.timer.
[arnaud@web ~]$ sudo systemctl status tp2_backup.timer
● tp2_backup.timer - Periodically run our TP2 backup script
   Loaded: loaded (/etc/systemd/system/tp2_backup.timer; enabled; vendor preset: disabled)
   Active: active (waiting) since Mon 2021-11-01 21:25:24 CET; 58s ago
  Trigger: Mon 2021-11-01 21:27:00 CET; 37s left

nov. 01 21:25:24 web.tp2.linux systemd[1]: Started Periodically run our TP2 backup script.
[arnaud@web ~]$ sudo borg list /srv/backup/repo/
Enter passphrase for key /srv/backup/repo:
tp2_backup_2021-11-01T19:56:54       Mon, 2021-11-01 19:56:56 [75829512fa4cd8c5fa5606eaba873fa5f81f34cb0e6c7c1a1ae143720494af46]
tp2_backup_2021-11-01T20:16:26       Mon, 2021-11-01 20:16:26 [f0b16444cec172f3b9e788e9e2f8bb21e6252f58a22bda03ad2ecdc43c31f6e8]
tp2_backup_2021-11-01T20:16:35       Mon, 2021-11-01 20:16:35 [4ec713e3feb5d2e1cfb528ab4a8de20c4ab4e446c07608b54a913e6cda29076b]
tp2_backup_2021-11-01T21:25:26       Mon, 2021-11-01 21:25:26 [9082693e9e287ded13a0d523c1ac87432fe6f7034d26688d6b687ad1fa3324aa]
tp2_backup_2021-11-01T21:26:04       Mon, 2021-11-01 21:26:04 [9dda4d99680449ce4a06f83025a3bce64adb4a44242e9135d6dc79beb63f426d]

[arnaud@web ~]$ sudo systemctl list-timers | grep tp2
Tue 2021-11-02 03:15:00 CET  5h 34min left Mon 2021-11-01 21:40:03 CET  28s ago      tp2_backup.timer             tp2_backup.service
```
Pour voir la configuration du service de backup de NetData (tous les matins à 3h15) c'est ici :
https://gitlab.com/Arnaudboy/tp_leo/-/tree/master/Linux/tp_linux_02_part2_b2/tp2_backup

## 5. Backup de base de données

Pour la partie "Création du script" je mets directement le résultat dans le repo -> https://gitlab.com/Arnaudboy/tp_leo/-/tree/master/Linux/tp_linux_02_part2_b2/tp2_backup

** 🌞 Restauration **
Après avoir récupérer le nom de l'archive :
```
[arnaud@db ~]$ sudo borg extract /srv/backup/repo::tp2_backup_2021-11-01T23:52:02
[arnaud@db ~]$ mysql -u root -p nextcloud < /srv/backup/home/arnaud/nextcloud.sql
```
Par soucis de simplicité mon script génère un fichier sql dans le home directory (le mot de passe est en claire dans le script pour aller plus vite, comme précédement pour borg j'aurais pu le coller dans un fichier autre fichier).

** 🌞 Unité de service **
```
[arnaud@db ~]$ systemctl status tp2_backup_db
● tp2_backup_db.service - Our own lil database backup service (TP2)
   Loaded: loaded (/etc/systemd/system/tp2_backup_db.service; disabled>
   Active: inactive (dead) since Tue 2021-11-02 01:24:01 CET; 55s ago
 Main PID: 7674 (code=exited, status=203/EXEC)

nov. 02 01:24:01 db.tp2.linux systemd[1]: Starting Our own lil databas>
nov. 02 01:24:01 db.tp2.linux systemd[1]: tp2_backup_db.service: Succe>
nov. 02 01:24:01 db.tp2.linux systemd[1]: Started Our own lil database>
[arnaud@db ~]$ systemctl status tp2_backup_db.timer
● tp2_backup_db.timer - Periodically run our TP2 backup script
   Loaded: loaded (/etc/systemd/system/tp2_backup_db.timer; disabled; >
   Active: active (waiting) since Tue 2021-11-02 01:23:13 CET; 1min 50>
  Trigger: Tue 2021-11-02 03:30:00 CET; 2h 4min left

nov. 02 01:23:13 db.tp2.linux systemd[1]: Started Periodically run our>

[arnaud@db ~]$ sudo systemctl list-timers | grep tp2
Tue 2021-11-02 03:30:00 CET  2h 3min left  Tue 2021-11-02 01:24:01 CET  2min 7s ago  tp2_backup_db.timer          tp2_backup_db.service
```

# III. Reverse Proxy

## 2. Setup simple

** 🌞 Tester !**

```
[arnaud@front ~]$ ss -anltp
State           Recv-Q           Send-Q                     Local Address:Port                     Peer Address:Port          Process
LISTEN          0                128                              0.0.0.0:80                            0.0.0.0:*
LISTEN          0                128                              0.0.0.0:22                            0.0.0.0:*
LISTEN          0                128                                 [::]:80                               [::]:*
LISTEN          0                128                                 [::]:22                               [::]:*
```
Nginx est en écoute sur le port 80 (HTTP).
Vérification de fonctionnement avec `curl`.
```
[arnaud@front ~]$ curl 127.0.0.1:80
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
    <title>Test Page for the Nginx HTTP Server on Rocky Linux</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
...
```

** 🌞 Explorer la conf par défaut de NGINX **

+ repérez l'utilisateur qu'utilise NGINX par défaut

```
[arnaud@front ~]$ cat /etc/nginx/nginx.conf | grep user
user nginx;

[arnaud@front ~]$ ps -ef | grep nginx
root       26770       1  0 15:56 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx      26771   26770  0 15:56 ?        00:00:00 nginx: worker process
```
L'utilisateur par défaut est nginx.

+ dans la conf NGINX, on utilise le mot-clé server pour ajouter un nouveau site

```
server {
        listen       80 default_server;
        listen       [::]:80 default_server;
        server_name  _;
        root         /usr/share/nginx/html;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        location / {
        }

        error_page 404 /404.html;
            location = /40x.html {
        }

        error_page 500 502 503 504 /50x.html;
            location = /50x.html {
        }
    }
```

+ par défaut, le fichier de conf principal inclut d'autres fichiers de conf

```
[arnaud@front ~]$ grep -o "\/.\+\.conf" /etc/nginx/nginx.conf | uniq
/usr/share/nginx/modules/*.conf
/etc/nginx/conf.d/*.conf
/etc/nginx/default.d/*.con
```
On peut aller un peu plus loin :
```
ls $(grep -o "\/.\+\.conf" /etc/nginx/nginx.conf | uniq)
ls: impossible d'accéder à '/etc/nginx/conf.d/*.conf': No such file or directory
ls: impossible d'accéder à '/etc/nginx/default.d/*.conf': No such file or directory
 /usr/share/nginx/modules/mod-http-image-filter.conf   /usr/share/nginx/modules/mod-mail.conf
 /usr/share/nginx/modules/mod-http-perl.conf           /usr/share/nginx/modules/mod-stream.conf
 /usr/share/nginx/modules/mod-http-xslt-filter.conf
```
Les deux premiers dossiers de la liste sont vides mais inclus par défaut, j'imagine que c'est ici que je devrais inclure des fichiers maison.

** 🌞 Modifier la conf de NGINX **
```
[arnaud@front ~]$ sudo systemctl restart nginx.service
[arnaud@front ~]$ curl 127.0.0.1:80
<!DOCTYPE html>
<html>
<head>
	<script> window.location.href="index.php"; </script>
	<meta http-equiv="refresh" content="0; URL=index.php">
</head>
</html>
```

# IV. Firewalling

## 2. Mise en place

Ma zone drop a inclut par défaut les interfaces enp0s8 (réseau privé hôte) et enp0s3 (NAT). Je n'ai pas réussi à retirer l'interface enp0s3.

### A. Base de données

```
[arnaud@db ~]$ sudo firewall-cmd --set-default-zone=drop
[arnaud@db ~]$ sudo firewall-cmd --new-zone=ssh --permanent
[arnaud@db ~]$ sudo firewall-cmd --zone=ssh --add-source=10.102.1.1/32 --permanent
[arnaud@db ~]$ sudo firewall-cmd --zone=ssh  --add-port=22/tcp --permanent
[arnaud@db ~]$ sudo firewall-cmd --new-zone=db --permanent
[arnaud@db ~]$ sudo firewall-cmd --zone=db --add-source=10.102.1.11 --permanent
[arnaud@db ~]$ sudo firewall-cmd --zone=db --add-port=3306/tcp --permanent
[arnaud@db ~]$ sudo systemctl restart firewalld.service
```
** 🌞 Montrez le résultat de votre conf avec une ou plusieurs commandes firewall-cmd **
```
[arnaud@db ~]$ sudo firewall-cmd --get-active-zones
db
  sources: 10.102.1.11/32
drop
  interfaces: enp0s3 enp0s8
ssh
  sources: 10.102.1.1/32

[arnaud@db ~]$ sudo firewall-cmd --get-default-zone
drop

[arnaud@db ~]$ sudo firewall-cmd --list-all --zone=drop
drop (active)
  target: DROP
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services:
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

[arnaud@db ~]$ sudo firewall-cmd --list-all --zone=ssh
ssh (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.1/32
  services:
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

[arnaud@db ~]$ sudo firewall-cmd --list-all --zone=db
db (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.11/32
  services:
  ports: 3306/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

## B. Serveur Web

```
[arnaud@web ~]$ sudo firewall-cmd --set-default-zone=drop
[arnaud@web ~]$ sudo firewall-cmd --new-zone=ssh --permanent
[arnaud@web ~]$ sudo firewall-cmd --zone=ssh --add-source=10.102.1.1/32 --permanent
[arnaud@web ~]$ sudo firewall-cmd --zone=ssh  --add-port=22/tcp --permanent
[arnaud@web ~]$ sudo firewall-cmd --new-zone=web --permanent
[arnaud@web ~]$ sudo firewall-cmd --zone=web --add-source=10.102.1.14/32 --permanent
[arnaud@web ~]$ sudo firewall-cmd --zone=web --add-port=80/tcp --permanent
[arnaud@web ~]$ sudo systemctl restart firewalld.service
```

** 🌞 Montrez le résultat de votre conf avec une ou plusieurs commandes firewall-cmd **
```
[arnaud@web ~]$ sudo firewall-cmd --get-active-zones
web
  sources: 10.102.1.14/32
drop
  interfaces: enp0s3 enp0s8
ssh
  sources: 10.102.1.1/32

[arnaud@web ~]$ sudo firewall-cmd --get-default-zone
drop

[arnaud@web ~]$ sudo firewall-cmd --list-all --zone=web
web (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.14/32
  services:
  ports: 80/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

## C. Serveur de backup

```
[arnaud@backup ~]$ sudo firewall-cmd --set-default-zone=drop
[arnaud@backup ~]$ sudo firewall-cmd --new-zone=ssh --permanent
[arnaud@backup ~]$ sudo firewall-cmd --zone=ssh --add-source=10.102.1.1/32 --permanent
[arnaud@backup ~]$ sudo firewall-cmd --zone=ssh  --add-port=22/tcp --permanent
[arnaud@backup ~]$ sudo firewall-cmd --new-zone=backup --permanent
[arnaud@backup ~]$ sudo firewall-cmd --zone=backup --add-source=10.102.1.11/32 --add-source=10.102.1.12/32 --permanent
[arnaud@backup ~]$ sudo firewall-cmd --zone=backup --add-port=2049/tcp --permanent
[arnaud@backup ~]$ sudo systemctl restart firewalld.service
```

** 🌞 Montrez le résultat de votre conf avec une ou plusieurs commandes firewall-cmd **
```
[arnaud@backup ~]$ sudo firewall-cmd --get-active-zones
backup
  sources: 10.102.1.11/32 10.102.1.12/32
drop
  interfaces: enp0s3 enp0s8
ssh
  sources: 10.102.1.1/32

[arnaud@backup ~]$ sudo firewall-cmd --get-default-zone
drop

[arnaud@backup ~]$ sudo firewall-cmd --list-all --zone=backup
backup (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.11/32 10.102.1.12/32
  services:
  ports: 2049/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

## D. Reverse Proxy

```
[arnaud@front ~]$ sudo firewall-cmd --set-default-zone=drop
[arnaud@front ~]$ sudo firewall-cmd --new-zone=ssh --permanent
[arnaud@front ~]$ sudo firewall-cmd --zone=ssh --add-source=10.102.1.1/32 --permanent
[arnaud@front ~]$ sudo firewall-cmd --zone=ssh  --add-port=22/tcp --permanent
[arnaud@front ~]$ sudo firewall-cmd --new-zone=proxy --permanent
[arnaud@front ~]$ sudo firewall-cmd --zone=proxy --add-source=10.102.1.0/24 --permanent
[arnaud@front ~]$ sudo firewall-cmd --zone=proxy --add-port=80/tcp --permanent
[arnaud@front ~]$ sudo systemctl restart firewalld.service
```

** 🌞 Montrez le résultat de votre conf avec une ou plusieurs commandes firewall-cmd **
```
[arnaud@front ~]$ sudo firewall-cmd --get-active-zones
front
  sources: 10.102.1.0/24 
drop
  interfaces: enp0s3 enp0s8
ssh
  sources: 10.102.1.1/32

[arnaud@front ~]$ sudo firewall-cmd --get-default-zone
drop

[arnaud@front ~]$ sudo firewall-cmd --list-all --zone=proxy
proxy (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.0/24
  services:
  ports: 80/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```
## E. Tableau récap

| Machine | IP          | Service                 | Port ouvert | IPs autorisées               |
|---------|-------------|-------------------------|-------------|------------------------------|
| Web     | 10.102.1.11 | nextcloud (server web)  | 80/tcp      | 10.102.1.14 (front)          |
|         |             | ssh                     | 22/tcp      | 10.102.1.1 (hôte)            |
| Db      | 10.102.1.12 | nextcloud (server web)  | 80/tcp      | 10.102.1.14 (front)          |
|         |             | ssh                     | 22/tcp      | 10.102.1.1 (hôte)            |
|         |             | MariaDB (server de bdd) | 3306/tcp    | 10.102.1.11                  |
| Backup  | 10.102.1.13 | ssh                     | 22/tcp      | 10.102.1.1 (hôte)            |
|         |             | nfs                     | 2049/tcp    | 10.102.1.(11 & 12) (web, db) |
| Front   | 10.102.1.14 | Reverse proxy (nginx)   | 80/tcp      | 10.102.1.11 (web)            |
|         |             | ssh                     | 22/tcp      | 10.102.1.1 (hôte)            |
