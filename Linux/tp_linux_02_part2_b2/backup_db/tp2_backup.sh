#!/bin/bash
# Simple backup script (linux tp2 1ère version du script)
# Arnaud 18/10/21

# Remember -> $@ = tous les arguments ($1 $2 $3)
# placholder pour les noms d'archives -> {now}, {utcnow}, {fqdn}, {hostname}, {user} 
# echo <PATH> | rev | cut -d '/' -f1 | rev

source /home/arnaud/backup_variables.sh
buDirectory=""
export BORG_PASSPHRASE="arnaud"

usage() {
    echo "-q (optionel) permet de modifier de façon permanente la quantité de backups conservés"
    echo "-d [CHEMIN VERS LE REPO] (obligatoire) indique le repository vers lequel vous voulez effectuer votre backup"
    echo " si aucun repository n'existe, entrez un chemin, un nouveau repository nommé  newRepository y sera créé il vous sera alors demander d'entrée une passphrase"
}
mysqldump -u root -proot nextcloud > /home/arnaud/nextcloud.sql
# Analyse des options
while getopts "hd:q:" OPTION; do
        case $OPTION in
                h)
                       # insert help directions 
                       usage
                       exit 0
                        ;;
                q)
                        # keep quantity in $OPTARG
                        echo "quantity=$OPTARG" > /srv/backup_variables.sh
                        source /srv/backup_variables.sh
                        ;;
                d)
                        buDirectory=$OPTARG 
                        ;;
        esac
done
shift $((OPTIND-1))

# backup uniquement des fichiers et dossiers existant
for arg in $@
do
        if [[ ! -d ${arg} && ! -f ${arg} ]]
        then
                >&2 echo -e "${arg} n'est pas un fichier ou un répertoire valide"
               exit 1
        fi  
done

# borg créé une archive avec tous les fichiers en arguments
borg create --compression zlib ${buDirectory}::tp2_backup_{now} /home/arnaud/nextcloud.sql
rm /home/arnaud/nextcloud.sql
# Conserve uniquement le nombre d'archives indiqué par l'utilisateur (si l'utilisateur ne l'a pas modifié ce chiffre est de 5)
if [[ $(borg list ${buDirectory}| grep -c 2) -gt ${quantity} ]]
    then  
            borg delete --first $(($(borg list ${buDirectory} | grep -c 2) - ${quantity})) ${buDirectory}
fi

