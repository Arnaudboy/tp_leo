# TP2 pt. 1 : Gestion de service (https://gitlab.com/it4lik/b2-linux-2021/-/tree/main/tp/2)
# I. Un premier serveur web
## 1. Installation
### 🌞 Installer le serveur Apache
```
[arnaud@web ~]$ sudo dnf install -y httpd

[arnaud@web ~]$ ls /etc/httpd/
conf  conf.d  conf.modules.d  logs  modules  run  state
...
```
### 🌞 Démarrer le service Apache
```
[arnaud@web ~]$ systemctl enable httpd.service
[arnaud@web ~]$ systemctl start httpd.service

[arnaud@web ~]$ systemctl list-unit-files --type service
...
httpd.service                              enabled

[arnaud@web ~]$ systemctl status httpd.service
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor prese>
   Active: active (running) since Wed 2021-09-29 17:50:35 CEST; 2min 24s ago

[arnaud@web ~]$ sudo firewall-cmd --add-port=80/tcp

[arnaud@web ~]$ sudo ss -alnpt
...
LISTEN    0         128                      *:80                      *:*        users:(("httpd",pid=863,fd=4),("httpd",pid=862,fd=4),("httpd",pid=861,fd=4),("httpd",pid=836,fd=4))
...
```
Une fois le firewall ouvert et après verification que le port utilisé est bien le 80 j'entre l'adresse ip de ma machine dans mon navigateur pour accéder au service apache depuis mon pc.
## 2. Avancer vers la maîtrise du service
### 🌞 Le service Apache...
+ donnez la commande qui permet d'activer le démarrage automatique d'Apache quand la machine s'allume
La commande suivante demande à systemd d'activer le service httpd "au démarrage" (quand c'est le moment dans la procédure de démarrage de la machine)
```
[arnaud@web ~]$ systemctl enable httpd.service
```
+ prouvez avec une commande qu'actuellement, le service est paramétré pour démarré quand la machine s'allume
```
[arnaud@web ~]$ systemctl list-unit-files --type=service | grep httpd
httpd.service                              enabled
```
+ affichez le contenu du fichier httpd.service qui contient la définition du service Apache
```
[arnaud@web ~]$ cat /usr/lib/systemd/system/httpd.service
#	[Service]
#	Environment=OPTIONS=-DMY_DEFINE

[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```
### 🌞 Déterminer sous quel utilisateur tourne le processus Apache
+ mettez en évidence la ligne dans le fichier de conf principal d'Apache (httpd.conf) qui définit quel user est utilisé
```
[arnaud@web ~]$ cat /etc/httpd/conf/httpd.conf | grep -i user
User apache
```
+ utilisez la commande ps -ef pour visualiser les processus en cours d'exécution et confirmer que apache tourne bien sous l'utilisateur mentionné dans le fichier de conf
```
[arnaud@web ~]$ ps -ef
root        1920       1  0 17:17 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1921    1920  0 17:17 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1922    1920  0 17:17 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1923    1920  0 17:17 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1924    1920  0 17:17 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
```
+ vérifiez avec un ls -al le dossier du site (dans /var/www/...) 
   + vérifiez que tout son contenu est accesible à l'utilisateur mentionné dans le fichier de conf
```
[arnaud@web ~]$ ls -al /var/www/
total 4
drwxr-xr-x.  4 root root   33 29 sept. 12:32 .
drwxr-xr-x. 22 root root 4096 29 sept. 12:32 ..
drwxr-xr-x.  2 root root    6 11 juin  17:35 cgi-bin
drwxr-xr-x.  2 root root    6 11 juin  17:35 html
```
Tout le dossier est en lecture et execution pour tout le monde.
### 🌞 Changer l'utilisateur utilisé par Apache
```
[arnaud@web ~]$ sudo useradd -d /usr/share/httpd -s /sbin/nologin -g apache newUser
```
Un petit nano sur /etc/httpd/conf/httpd.conf pour modifier la ligne user car je suis pas encore assez bon pour le faire avec un sed...
Puis
```
[arnaud@web ~]$ ps -ef
root        1819       1  1 18:05 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
newUser     1820    1819  0 18:05 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
newUser     1821    1819  0 18:05 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
newUser     1822    1819  0 18:05 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
newUser     1823    1819  0 18:05 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
```
### 🌞 Faites en sorte que Apache tourne sur un autre port
Je choisi le port 8888 je modifie la ligne LISTEN de /etc/httpd/conf/httpd.conf et je modifie la configuration de firewall-cmd.
```
sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 8888/tcp
  ...
```
+ prouvez avec une commande ss que Apache tourne bien sur le nouveau port choisi
```
[arnaud@web ~]$ sudo ss -alnpt
LISTEN    0         128                      *:8888                    *:*        users:(("httpd",pid=2155,fd=4),("httpd",pid=2154,fd=4),("httpd",pid=2153,fd=4),("httpd",pid=2150,fd=4))
```
+ vérifiez avec curl en local que vous pouvez joindre Apache sur le nouveau port
```
[arnaud@web ~]$ curl localhost:8888
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
```
FICHIER DE CONF ENREGISTRE SOUS httpd.conf_UnNouveauFichier (j'ai remis Apache en écoute sur le port 80 et le user par défaut).
# II. Une stack web plus avancée
## 2. Setup
### A. Serveur Web et NextCloud
### 🌞 Install du serveur Web et de NextCloud sur web.tp2.linux
```
[arnaud@db ~]$ sudo dnf install epel-release -y
[arnaud@db ~]$ sudo dnf update -y
[arnaud@db ~]$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm -y
[arnaud@db ~]$ sudo dnf module enable php:remi-7.4 -y
[arnaud@db ~]$ sudo dnf install httpd mariadb-server vim wget zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp -y
[arnaud@db ~]$ sudo mkdir /etc/httpd/sites-available
[arnaud@db ~]$ sudo nano /etc/httpd/sites-available/linux.tp2.web (edit des noms de domaines)
[arnaud@db ~]$ sudo mkdir /etc/httpd/sites-enabled
[arnaud@db ~]$ sudo ln -s /etc/httpd/sites-available/linux.tp2.web /etc/httpd/sites-enabled/
[arnaud@db ~]$ sudo nano /etc/opt/remi/php74/php.ini (edit de la timezone)
[arnaud@db ~]$ sudo wget https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
[arnaud@db ~]$ sudo unzip nextcloud-21.0.1.zip
[arnaud@db ~]$ cd nextcloud/
[arnaud@db ~]$ sudo cp -Rf * /var/www/sub-domains/linux.tp2.web/html/
[arnaud@db ~]$ sudo chown -Rf apache.apache /var/www/sub-domains/linux.tp2.web/html/
[arnaud@db ~]$ sudo nano /etc/httpd/conf/httpd.conf (ajout de `IncludeOptional sites-enabled/*` à la fin du fichier)
[arnaud@db ~]$ reboot
```
FICHIER DE CONF ENREGISTRE SOUS httpd.conf_LeRetourDuFichier
### B. Base de données
```
[arnaud@db ~]$ sudo dnf install epel-release -y
[arnaud@db ~]$ sudo dnf update -y
[arnaud@db ~]$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm -y
[arnaud@db ~]$ sudo dnf module enable php:remi-7.4 -y
[arnaud@db ~]$ sudo dnf install httpd mariadb-server vim wget zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp -y
[arnaud@db ~]$ sudo dnf install -y mariadb-server
[arnaud@db ~]$ mysql_secure_installation
[arnaud@db ~]$ sudo systemctl enable mariadb
[arnaud@db ~]$ sudo systemctl restart mariadb
[arnaud@db ~]$ ss -alnpt
LISTEN          0                80                                     *:3306                                *:*
```
MariaDB est en écoute sur le port 3306.
### 🌞 Préparation de la base pour NextCloud
+ trouver une commande qui permet de lister tous les utilisateurs de la base de données
```
SELECT User FROM mysql.user;
```
Je l'ai entrée sur db avec un utilisateur root, depuis web et l'utilisateur nextcloud la commande m'a été refusé :
```
ERROR 1142 (42000): SELECT command denied to user 'nextcloud'@'10.102.1.11' for table 'user'
```
nextcloud a tous les droits sur la bdd nextcloud mais pas sur le server.
### C. Finaliser l'installation de NextCloud
+ modifiez votre fichier hosts
```
(base) ➜  ~ sudo cat /etc/hosts
...
# B2 tp2 de linux
10.102.1.11 web.tp2.linux
```
### 🌞 Exploration de la base de données
Pour connaître le nombre de rows avec une requête SQL j'ai trouvé deux solution :
```
SELECT COUNT(*) from information_schema.tables where TABLE_SCHEMA = 'nextcloud';
```
ou encore
```
USE nextcloud SELECT FOUND_ROWS();
```
Chez moi 99 tables ont été créées.

| Machine           | IP            | Masque            | Service                 | Port ouvert       | IP autorisées|
|-------------------|---------------|-------------------|-------------------------|-------------------|--------------|
| web.tp2.linux     | `10.102.1.11` | `255.255.255.0`   | Serveur Web             | 80/tcp - 22/tcp   |toutes les ip |
| db.tp2.linux      | `10.102.1.12` | `255.255.255.0`   | Serveur Base de Données | 3306/tcp - 22/tcp |`10.102.1.11` |
web.tp2.linux autorise n'importe quelle ip à utiliser le port 80 et 22, pour le port 22 il faudra quand avoir une clé ou un mot de passe valide pour établir une connexion.