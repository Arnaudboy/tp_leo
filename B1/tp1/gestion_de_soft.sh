#!/bin/bash
#auteur : Arnaud Boyer
#30 octobre 2020

# Un gestionnaire de paquets automatise le téléchargement, 
# l'installation et la desinstallation de logiciel. 
# Le gestionnaire est capable de rechercher en ligne, 
# suivant une liste pré-établie et fiable, 
# les dépôts sur lesquels les éditeurs de logiciels ont déposé 
# leurs code source; sécurisant ainsi 
# le processus de téléchargement.

brew list
echo ""
echo "**********"
brew tap