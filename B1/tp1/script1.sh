#!/bin/bash
#auteur : Arnaud Boyer
#19 octobre 2020
echo "Nom de la machine :"
hostname
echo ""
echo "**********"
echo "Adresse IP principale"
ipconfig getifaddr en0
echo ""
echo "**********"
echo "OS et version de l'OS"
sw_vers
echo ""
echo "**********"
echo "Date et heure d'allumage"
uptime
echo ""
echo "**********"
if $(softwareupdate -l | grep "Label: macOS" | awk '{ print $5 }') = $(sw_vers -productVersion) ]
then
       echo "OS is up to date";
else
       echo "OS not up to date";
fi
echo ""
echo "**********"
top -l 1 | grep -E "^Phys"
echo ""
echo "**********"
top -l 1 | grep -E "^CPU"
echo ""
echo "**********"
echo "Espace disque total :  $(df -h | grep -E "/dev/disk1s1" | awk '{print $2}')"
echo "Espace disque libre : $(df -h | grep -E "/dev/disk1s1" | awk '{print $4}')"
echo ""
echo "**********"
# uncomment the next line to read ALL users
# cat /etc/passwd | awk -F : '{print $1}'
# This command display 5 firsts users
echo "First 5 users :"
cat /etc/passwd | awk -F : '{print $1}' | sed -n '11,15p'
echo ""
echo "**********"
ping -c 8 10.33.3.253 | grep 'round-trip' | cut -d'/' -f5