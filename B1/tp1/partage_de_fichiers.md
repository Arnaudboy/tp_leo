# Partage de fichier protocole NFS

## Configuration du server

### Le TCC

le TCC est un framework Apple. Il a été spécifiquement développé pour limiter les accés aux périphériques et au(x) disque(s) des applications. il possède principalement deux bases de données et son propre daemon.

Le TCC possède une configration précise et autorise le partage de fichiers si et seulement si, ceux-ci sont placé au path #HOME/Projects/. La création du dossier Projects est donc un préalable au partage de fichiers.

### Le nfsd

Autre daemon spécifique pour le protocole nfs: le nfsd. C'est lui qui contient le protocole, il demande simplement une précision sur le dossier à partager, les droits du clients et son adresse IP. On peut autoriser un ou plusieurs clients à accéder au dossier, voir un groupe entier. Ces informations sont stockées dans un fichier situé au path /etc/exports. Le fichier doit être créé s'il n''est pas présent. Dans ce fichier et pour le cas qui nous intéresse j'ai saisi les données suivantes :  

`/Users/arnaudboyer/Projects/toto -rw -mapall=arnaudboyer 192.168.120.50`

Je peux vérifié que le nfsd est bien lancé avec `nfsd status`

## Configuration du client

Une fois centOS conrrectement installé j'active la carte réseau enp0s8 grâce à `ifup`. Je sélectionne ou je crée le dossier à monter, c'est à dire le dossier dans lequel je vais retrouver les fichiers partagés par le server, et je lance la commande suivante (en root ou en superutilisateur) :

`mount -t nfs ip_du_server:/path/to/Projects path/to/mounted_files`

Dans mon cas précis :

`mount -t nfs 192.168.120.1:/Users/arnaudboyer/Projects/toto /mnt/nfs/var/nfsshare`

Je peux vérifier que le dossier est bien monté grâce à la commande `showmount` et vérifier que tout est fonctionnel en créant un fichier depuis ma vm.

# SSH

Last login: Mon Nov  9 16:42:33 on ttys001

The default interactive shell is now zsh.
To update your account to use zsh, please run `chsh -s /bin/zsh`.
For more details, please visit https://support.apple.com/kb/HT208050.
iMacdeArnaud-2:~ arnaudboyer$ zsh

➜  ~ ssh root@192.168.120.50

root@192.168.120.50's password:

Last failed login: Sun Nov  8 19:22:36 CET 2020 from 192.168.120.1 on ssh:notty

There were 5 failed login attempts since the last successful login.
Last login: Wed Nov  4 19:42:00 2020 from 192.168.120.1

[root@localhost ~]# ip a

1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000

    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00

    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever

2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000

    link/ether 08:00:27:56:d3:45 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global noprefixroute dynamic 
    
    enp0s3
       valid_lft 86346sec preferred_lft 86346sec
    inet6 fe80::7d1a:361f:8750:d99a/64 scope link noprefixroute
       valid_lft forever preferred_lft forever

3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000

    link/ether 08:00:27:50:57:0a brd ff:ff:ff:ff:ff:ff

    inet 192.168.120.50/24 brd 192.168.120.255 scope global noprefixroute dynamic enp0s8
       valid_lft 572sec preferred_lft 572sec
    inet6 fe80::5e3b:3ff2:1592:65ee/64 scope link noprefixroute
       valid_lft forever preferred_lft forever

[root@localhost ~]#