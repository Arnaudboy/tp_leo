#!/bin/bash
#auteur : Arnaud Boyer
#31 octobre 2020
echo "**** HOST OS ****"
echo
echo -e "Nom de la machine: \n$(hostname)"
echo ""
echo "**********"
echo -e "OS et version: \n$(sw_vers)"
echo ""
echo "**********"
echo -e "Architecture de l'OS: \n$(uname -m)"
echo ""
echo "**********"
echo -e "Modèle et quantité de la RAM :\n$(system_profiler SPMemoryDataType | egrep "Part|Speed|Size")"
#echo "quantité de la RAM: $(expr "$(system_profiler SPMemoryDataType | grep "Size" | awk '{print $2}' | sed -n '1p')" + "$(system_profiler SPMemoryDataType | grep "Size" | awk '{print $2}' | sed -n '2p')") GB"
echo "**** DEVICES ****"
echo
echo -e "Type du processeur: \n$(system_profiler SPHardwareDataType | grep "Processor")"
echo "modèle : $(sysctl -n machdep.cpu.brand_string | awk '{print $3}')"
# sur ma machine je possède un processeur 4 coeurs. Il s'agit d'un i5 (brand modifier, plus le chiffre après le i est élevé plus le processeur est supposé performant)
# le 8 après le - indique la génération du processeur
# le 257 est le sku, un sku élevé indique plus de features
# le U est le suffixe, il indique que mon processeur est optimisé pour les ordinateurs portables
echo ""
echo "**********"
echo "Type de carte graphique:$(system_profiler SPDisplaysDataType | grep "Chipset Model:" | cut -d ":" -f2)"
echo ""
echo "**********"
echo "Nom et modèle du disque dur:$(diskutil info disk0 | grep "Device / Media Name:" | cut -d ":" -f2)"
echo ""
echo "**********"
diskutil list
#Je possède 1 disque dur lui même découpé en 3 partition disk0s1 qui comprends mon EFI et mon boot loader
# disk0s2, disque dur virtuel contenant mes données (disk1), et plusieurs partitions contenant des sous-partitions utiles à mon OS
# disk3 contient l'Apple Partition Map (APM) est une organisation d'OS de bas niveau
# le systeme de fichier de disk0 est EFI
# le systeme de fichier de disk1 est APFS
echo "**** USERS ****"
echo
echo ""
echo "**********"
echo "liste des utilisateur (les 5 premiers pour plus de lisibilité) :"
cat /etc/passwd | awk -F : '{print $1}' | cut -d '_' -f2 | sed -n '11,15p' # Effacer (| sed -n '11,15p') pour la liste complète
# root est l'utilisateur full admin 
echo "**** PROCESSUS ****"
echo
#ps aux
#uncommment the line above to see the full process launched on your computer
ps -p 1
#launchd est un framework pour lancer, stopper et gérer les daemons(programme fonctionnant en arrière-plan), les applications, les process et les scripts.
ps -p 103
#syslogd stocke les logs -> les messages des événements survenu lors de l'utilisation de l'OS ou d'un logiciel
ps -p 104
# UserEventAgent est un daemon qui charge certains plugin systeme qui ne peuvent pas être directement afichés par launchd
ps -p 109
#kextd est un daemon, il gère les requêtes du kernel
ps -p 131
#diskarbitrationd détecte les connections et notifie le client de l'apparition d'un nouveau systeme de stockage de données.
echo "**** NETWORK ****"
echo
system_profiler SPNetworkDataType
#ifconfig affiche une liste plus complète mais moins lisible de toutes les cartes réseaux.
# Je possède une carte de loopback servant a tester mon matériel
# une carte Ethernet pour les connexions filaires
# une carte wifi
# une carte bluetooth
netstat -a
# la plupart sont des connections utilisant les ports 80 (http) et 443 (https). Les programmes qui tournent derrière sont donc les navigateurs web.